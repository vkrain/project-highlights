

# 项目展示

> 个人项目经验丰富，涵盖web端、app端、pc端等项目，以及参与过sass、cms等系统的开发



## web端项目

### 01、智慧工地综合信息管理平台

#### 项目简介：

> 智慧工地综合信息管理平台是一款集成多种信息技术的数字化施工管理系统，围绕人员、设备、材料、方法、环境等关键要素，通过实时数据采集、共享和智能分析，实现施工现场的监控、协同和风险预控，提升工程管理效率与安全，助力建筑行业数字化转型。



#### 技术选型概览：

web端：Vue3 + Element-plus + Echarts

后端：Java+ Spring Cloud Alibaba

DB：mysql + redis



#### 项目截图：

<img src="./gif/project-1.gif"></img>



### 02、五邑大学创新创业平台

#### 项目简介：

> 五邑大学创新创业系统平台是一个集赛事管理、公告发布、赛事评分和校企资源整合于一体的综合服务平台，



#### 技术选型概览：

前后台： Vue2 + Vue-element-admin+ Echarts

后端：PHP + Laravel 

DB：mysql + redis



[点击预览](http://url.vky.icu/w )

#### 项目截图：

<img height="1080px" src='./screenshots/web-2.png'></img>

<img src='./screenshots/web-2-1.png'></img>





### 03、数字出版平台

#### 技术选型概览：

web端： Vue2 + Element-ui + jQuery + Bootstrap + Swiper

后端：PHP + Laravel 

DB：mysql + redis



#### 项目截图：

<img src='./screenshots/web-3.png'></img>



----



## 移动端项目

### 04、智慧工地-app端

#### 技术选型概览：

app端： Vue3 + Uni-app + Wot-Design-Uni



#### 项目演示：

<img src="http://cdn.vky.icu/project/project-3.gif" height="500px"></img>

#### 项目截图：

<p align='center'>
<img alt="" src="./screenshots/app.png"style="display:inline-block; height:400px;">
</p>




### 05、自助洗车-安卓终端

**项目简介：**

自助洗车终端是一款安卓触控终端，该终端通过安卓串口与车机系统通信，控制洗车过程中的出水和泡沫喷洒，为洗车场所提供智能化运营支持。

#### 技术选型概览：

app端： Vue3 + Uni-app + uvui



#### 项目演示：

<img src="http://cdn.vky.icu/project/project-2.gif"></img>

#### 项目截图：

<p align='center'>
<img alt="" src="./screenshots/terminal.png"style="display:inline-block; height:auto;">
</p>




### 06、摩彩app

#### 技术选型概览：

app端： Vue3 + Uni-app + threejs



#### 项目演示：

<img src="http://cdn.vky.icu/project/project-4.gif" height="500px"></img>





## pC端项目


#### 07、摩彩-桌面端

**项目简介：**

​	摩彩3d皮肤影像分析项目是一款皮肤影像分析数据管理平台，为机构提供影像采集、档案管理、数据分析的一站式服务，使得咨询师、医生和运营人员能够轻松地访问和分析顾客的皮肤数据，提供更专业的皮肤管理建议。



#### 技术选型概览：

 Vue3 +Electron + Element-plus + threejs



#### 项目演示：

<img src="http://cdn.vky.icu/project/project-5.gif" height="500px"></img>



<img src="http://cdn.vky.icu/project/project-6.gif" height="500px"></img>

<p align='center'>
<img alt="" src="./screenshots/face.png"style="display:inline-block; height:auto;">
</p>


## 小程序项目

#### 08、璟胜ai

**项目介绍：**

AI效率工具，啥都能写。周报、论文、小红书、AI问答、翻译、短视频脚本、带货文案、朋友圈文案 #人工智能 #AI写作 #AI #璟胜AI



**技术选型概览：**

微信端：uniapp + websocket + uview2.0

后端：goland + gin

DB：mysql+redis



**扫码预览**

<img src="./screenshots/wx-1.png"></img>

<img src="./screenshots/wx-1-1.png" height="400px"></img>





## Saas平台小程序

#### 项目简介

璟胜科技自研saas商城平台，包含外卖、电商、跑腿



#### 技术选型概览：

微信端：uniapp + jweixin-module

后台：layui + thinkphp

DB：mysql+redis

 

#### **案例1：sass-商城**

**扫码预览**

<img src="./screenshots/wx-2.png"></img>



<img src="./screenshots/wx-2-1.png" height="400px"></img>

<img src="./screenshots/wx-bg.png" height="400px"></img>

#### **案例2：**Sass-洗车平台

**扫码预览**

<img src="./screenshots/wx-3.png"></img>

<img src="./screenshots/wx-3-1.png" height="400px"></img>

<img src="./screenshots/wx-bg2.png" height="400px"></img>



## Cms建站项目

**技术选型概览：**

网页端：html + css + jQuery

后台：php + fastadmin 

DB：mysql



#### 案例1-国外站

[点击预览](http://url.vky.icu/2)

<img src="./screenshots/cms-1.png" height="400px"></img>

 

#### 案例2-哈宇音频官网

[点击预览](http://url.vky.icu/S)

<img src="./screenshots/cms-2.png" height="400px"></img>

#### 相关后台

<img src="./screenshots/cms-3.png" height="400px"></img>
